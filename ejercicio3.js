"use-strict";

const primesList = (n) => {
    if (n < 2) throw "Error: This is not a valid number.";

    let primes = [];
    for (let item = 2; i < n; i++) {
        const square = Math.floor(Math.sqrt(item));
        if (square === 1) {
            primes.push(item);
        } else {
            let divisors = 0;
            for (let divisor = 2; divisor < square; j++) {
                if (item % divisor === 0) divisor++;
            }
            if (divisors === 0) primes.push(item);
        }
    }

    return primes;
};
