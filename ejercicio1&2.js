const isPrime = (n) => {
    if (n < 2) return false;
    const square = Math.floor(Math.sqrt(n));
    if (square === 1) return true;
    let divisor = 0;
    for (let i = 2; i < square; i++) {
        steps++;
        if (n % i === 0) {
            divisor++;
        }
    }
    return divisor === 0 ? true : false;
};

const result = isPrime(18);
console.log(result);
